/**
* @store source file with functions for hotel-billing
* @author VizCreations
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hbll.h"

void show_config_(HBll *hbill) {
	OWN *owner;
	CFG *config;

	config = &hbill->config;
	owner = &config->owner;

	printf("Store Name: %s\n", config->name);
	printf("Registration ID: %s\n", config->id);
	printf("Store Address: %s\n", config->addr);
	printf("Established: %s\n", config->estdate);
	printf("Service Tax: %.2lf\n", config->servicetax);
	printf("VAT Tax: %.2lf\n", config->vattax);

	printf("Owner: %s\n", owner->name);
	printf("Owner Address: %s\n", owner->addr);
}

void scan_config_(HBll *hbill, FILE *fp) {
	int i=0,k=0,l=0;

	CFG *config;
	OWN *owner;
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	char addr[MAX_STR];
	char estdate[MAX_INPUT];
	double servicetax;
	double vattax;
	char oname[MAX_INPUT];
	char oaddr[MAX_STR];
	char *token;
	char delim[] = ";";
	char line[MAX_LINE];
	int lineno = 0;

	while(fgets(line, MAX_LINE+1, fp) != NULL) {
		lineno++;
		if(lineno == 1) {
			if(strlen(line) > 0) {
				token = strtok(line, delim);
				if(token != NULL) {
					strcpy(id, trim(token, MAX_INPUT));
				} else strcpy(id, "ABC000 ``INVALID ID``");
			}
		} else if(lineno == 2) {
			token = strtok(line, delim);
			if(token != NULL)
				strcpy(name, trim(token, MAX_INPUT));
			else strcpy(name, "UNKNOWN ``INVALID NAME``");
		} else if(lineno == 3) {
			token = strtok(line, delim);
			if(token != NULL)
				strcpy(addr, trim(token, MAX_STR));
			else
				strcpy(addr, "UNKNOWN ``INVALID ADDRESS``");
		} else if(lineno == 4) {
			token = strtok(line, delim);
			if(token != NULL)
				strcpy(estdate, trim(token, MAX_INPUT));
			else
				strcpy(estdate, "0000-00-00 00:00:00 ``INVALID DATE``");
		} else if(lineno == 5) {
			token = strtok(line, delim);
			if(token != NULL)
				servicetax = atof(trim(token, MAX_INPUT));
			else
				servicetax = 0.0;
		} else if(lineno == 6) {
			token = strtok(line, delim);
			if(token != NULL)
				vattax = atof(trim(token, MAX_INPUT));
			else vattax = 0.0;
		} else if(lineno == 8) { // #7 is for a new-line separation
			token = strtok(line, delim);
			if(token != NULL)
				strcpy(oname, trim(token, MAX_INPUT));
			else strcpy(oname, "UNKNOWN ``INVALID OWNER NAME``");
		} else if(lineno == 9) {
			token = strtok(line, delim);
			if(token != NULL) {
				strip_newline(token, MAX_STR);
				strcpy(oaddr, trim(token, MAX_STR));
			} else
				strcpy(oaddr, "UNKNOWN ``INVALID OWNER ADDRESS``");
		}

		if(lineno == 9) break; // Come out of loop
	}

	config = &hbill->config;
	strcpy(config->id, id);
	strcpy(config->name, name);
	strcpy(config->addr, addr);
	strcpy(config->estdate, estdate);
	config->servicetax = servicetax;
	config->vattax = vattax;

	owner = &config->owner;
	strcpy(owner->name, oname);
	strcpy(owner->addr, oaddr);
}

int save_config_(HBll *hbill) {
	FILE *fp;
	boolean success = FALSE;
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	char addr[MAX_STR];
	char estdate[MAX_INPUT];
	double servicetax;
	double vattax;
	OWN *owner;

	char oname[MAX_INPUT];
	char oaddr[MAX_STR];

	if(file_exists_(CONFIGFILE))
		unlink(CONFIGFILE); // Delete old configuration

	fp = fopen(CONFIGFILE, "w+");
	if(fp) {

		/** Write to file using program data */
		strcpy(id, hbill->config.id);
		strcpy(name, hbill->config.name);
		strcpy(addr, hbill->config.addr);
		strcpy(estdate, hbill->config.estdate);
		servicetax = hbill->config.servicetax;
		vattax = hbill->config.vattax;
		owner = &hbill->config.owner;

		strcpy(oname, owner->name);
		strcpy(oaddr, owner->addr);

		// Writing

		fprintf(fp, "%s\t%c\t%s\n", id, ';', "Registration ID");
		fprintf(fp, "%s\t%c\t%s\n", name, ';', "Store Name");
		fprintf(fp, "%s\t%c\t%s\n", addr, ';', "Store Address");
		fprintf(fp, "%s\t%c\t%s\n", estdate, ';', "Established Date");
		fprintf(fp, "%.2lf\t%c\t%s\n", servicetax, ';', "Service Tax");
		fprintf(fp, "%.2lf\t%c\t%s\n", vattax, ';', "VAT Tax");

		fprintf(fp, "\n");
		fprintf(fp, "%s\t%c\t%s\n", oname, ';', "Owner's Name");
		fprintf(fp, "%s\t%c\t%s\n", oaddr, ';', "Owner's Address");

		success = TRUE;
		fclose(fp);
	}
	return success;
}
