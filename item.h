/**
* @item header file for hotel billing
* @author VizCreations
*
*/

boolean item_qty_overflow_(HBll *, int);
void save_item_(HBll *, int);
void edit_oitem_(HBll *, char *, int);
void save_item_qty_(HBll *, int);
boolean save_oitems_(HBll *);
void add_item_(HBll *, char *, int);
void edit_item_(HBll *, int, char *, int);
void edit_item_qty_(HBll *, int, int );
void edit_item_price_(HBll *, int, double);
void edit_item_weight_(HBll *, int, double);
boolean save_items_(HBll *);
boolean del_item_(HBll *);
void del_oitem(HBll *);
boolean itm_key_exists_(HBll *, int);
boolean itm_ord_key_exists_(HBll *, int);
boolean itm_ords_key_exists_(HBll *, int);
void scan_olineitem(HBll *, char *, int);
void show_items_(HBll *);
