/**
* @company source file with functions for hotel-billing
* @author VizCreations
*
*/

void add_comp_(HBll *, char *);
void save_comp_(HBll *, char, char *);
void show_acc_(HBll *);
void show_comps_(HBll *);
void save_comps_(HBll *);
void scan_comp_(HBll *, FILE *);
