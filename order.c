/**
* @order functions source file for hotel-billing
* @author vizcreations
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include "hbll.h"

void show_order_(HBll *hbill) {
	int orderid = 0;
	char filename[MAX_FILEPATH];
	FILE *fp;
	ITM *item;

	int i = 0;
	int itemcount = 0;
	time_t ticks;
	double price = 0.0;
	double tot = 0.0;
	double stax = 0.0;
	double vatx = 0.0;
	char storename[MAX_INPUT];
	char storeaddr[MAX_STR];
	char dated[MAX_INPUT];
	char custname[MAX_INPUT];
	char custaddr[MAX_STR];
	ORD *order;

	ticks = time(NULL);

	orderid = hbill->curorderid;
//	order = &hbill->order[orderid]; // Already negated for index
	order = hbill->order; // First order in list => already a pointer
	itemcount = hbill->order[orderid].itemcount;
	stax = hbill->config.servicetax;
	vatx = hbill->config.vattax;
	if(itemcount>0) {
		strcpy(storename, hbill->config.name);
		strcpy(storeaddr, hbill->config.addr);
		strcpy(custname, order->cust.name);
		strcpy(custaddr, order->cust.address);
		strcpy(dated, ctime(&ticks));
		printf("Store/Business Name: %s\nAddress: %s\n\nCustomer: %s\nAddress: %s\nDated: %.24s\n", 
			storename, storeaddr, custname, custaddr, dated);
		puts("+--------+----------+---------------------+----------------+---------+");
		printf("| %-7s| %-9s| %-20s| %-15s| %-7s |\n",
				"ID", "ITEM-ID", "NAME", "QTY", "PRICE");
		puts("+--------+----------+---------------------+----------------+---------+");
//		while(i<itemcount) {
/*		while(
			item = &hbill->order[orderid].item[i];
			price = hbill->invt.item[atoi(item->id)-1].sprice*item->quantity;
			tot += price;
			printf("| %-7d| %-9s| %-20s| %-15d| %7.2lf |\n",
				i+FIRSTOITEMID,
				item->id,
				item->name,
				item->quantity,
				price);
			++i;
		}
*/		tot += stax + vatx;
		puts("+--------+----------+---------------------+----------------+---------+");
		printf("| %-7s| %-9s| %-20s| %-15s| %7.2lf |\n",
			"", "", "", "Service tax", stax);
		//puts("+--------+----------+---------------------+----------------+---------+");
		printf("| %-7s| %-9s| %-20s| %-15s| %7.2lf |\n",
			"", "", "", "VAT tax", vatx);
		//puts("+--------+----------+---------------------+----------------+---------+");
		printf("| %-7s| %-9s| %-20s| %-15s| %7.2lf |\n",
			"", "", "", "Total", tot);
		puts("+--------+----------+---------------------+----------------+---------+");
	} else puts("No items for this order..");
}

int print_order_(HBll *hbill, int save_to_file) {
	int orderid = 0;
	char filename[MAX_FILEPATH];
	FILE *fp;
	ITM *item;

	int i = 0;
	int itemcount = 0;
	time_t ticks;
	double price = 0.0;
	double tot = 0.0;
	double stax = 0.0;
	double vatx = 0.0;
	int rando;
	char cmd[MAX_CMD];
	char storename[MAX_INPUT];
	char storeaddr[MAX_STR];
	char dated[MAX_INPUT];
	char custname[MAX_INPUT];
	char custaddr[MAX_STR];
	ORD *order;
	ticks = time(NULL);
	srand(ticks);

	rando = rand();

	orderid = hbill->curorderid;
	order = &hbill->order[orderid]; // Already negated for index
	itemcount = hbill->order[orderid].itemcount;
	stax = hbill->config.servicetax;
	vatx = hbill->config.vattax;
	filename[0] = '\0';
	sprintf(filename, "%s%c%d%c%s", TMPDIR, DS, rando, '.', "waste");
	sprintf(cmd, "lpr %s", filename);

	fp = fopen(filename, "w+");
	if(fp) {
		if(itemcount>0) {
			strcpy(storename, hbill->config.name);
			strcpy(storeaddr, hbill->config.addr);
			strcpy(custname, order->cust.name);
			strcpy(custaddr, order->cust.address);
			strcpy(dated, ctime(&ticks));

			strip_newline(storeaddr, MAX_STR);
			strip_newline(dated, MAX_STR); // These two have newlines added after reading file
			fprintf(fp, "Store/Business Name: %s\nAddress: %s\n\nCustomer: %s\nAddress: %s\nDated: %s\n", 
				storename, storeaddr, custname, custaddr, dated);
			fprintf(fp, "+--------+----------+---------------------+----------------+---------+\n");
			fprintf(fp, "| %-7s| %-9s| %-20s| %-15s| %-7s |\n",
					"ID", "ITEM-ID", "NAME", "QTY", "PRICE");
			fprintf(fp, "+--------+----------+---------------------+----------------+---------+\n");
/*			while(i<itemcount) {
				item = &hbill->order[orderid].item[i];
				price = hbill->invt.item[atoi(item->id)-1].sprice*item->quantity;
				tot += price;
				fprintf(fp, "| %-7d| %-9s| %-20s| %-15d| %7.2lf |\n",
					i+FIRSTOITEMID,
					item->id,
					item->name,
					item->quantity,
					price);
				++i;
			}
*/			tot += stax + vatx;
			fprintf(fp, "+--------+----------+---------------------+----------------+---------+\n");
			fprintf(fp, "| %-7s| %-9s| %-20s| %-15s| %7.2lf |\n",
				"", "", "", "Service tax", stax);
			//puts("+--------+----------+---------------------+----------------+---------+");
			fprintf(fp, "| %-7s| %-9s| %-20s| %-15s| %7.2lf |\n",
				"", "", "", "VAT tax", vatx);
			//puts("+--------+----------+---------------------+----------------+---------+");
			fprintf(fp, "| %-7s| %-9s| %-20s| %-15s| %7.2lf |\n",
				"", "", "", "Total", tot);
			fprintf(fp, "+--------+----------+---------------------+----------------+---------+\n");

			fprintf(fp, "\n\n");
			fprintf(fp, "+--------+----------+---------------------+----------------+---------+\n");
			fprintf(fp, "| %-7s  %-9s  %-20s  %22s  %2s  \n",
				"", "", "", "Customer signature ", "|");
			fprintf(fp, "+--------+----------+---------------------+----------------+---------+\n");
		} else {
			fputs("No items for this order!\n", stderr);
		}

		fclose(fp);
		// File already created
		if(file_exists_(filename)) {
			if(save_to_file == FALSE) {
				puts("Printing..");
				system(cmd);
				unlink(filename); // Remove temporary file
				puts("done.");
			} else {
				printf("Order saved to file: %s\n", filename);
			}
		}
	} else {
		puts("Error printing to file!..");
		return -3;
	}
	return 0;
}

void show_orders_(HBll *hbill) {
	// Display from flat file
	FILE *fp;
	char filename[MAX_FILEPATH];
	char line[MAX_LINE];

	/** Local variables */
	int orderid = 0;
	int ordercount = 0;
	int itemcount = 0;
	char itemname[MAX_INPUT];
	int itemqty = 0;
	double itemprice = 0.0;
	double itemweight = 0.0;

	char custname[MAX_INPUT];
	char custemail[MAX_INPUT];
	char custaddr[MAX_STR];
	int custage = 0;
	char custsex = 'm';
	char timestamp[MAX_INPUT];
	int i = 0, j = 0, k = 0;
	double otot = 0.0; // Single order total
	double totprice = 0.0;
	double servicetax = 0.0;
	double vattax = 0.0;

	time_t ticks;

	ORD *order;
	int itemcnt = 0;
	int itemid = 0;
	int itemoqty = 0;
	ITM *item;
	servicetax = hbill->config.servicetax;
	vattax = hbill->config.vattax;
	if(hbill->ordercount > 0) {
		puts("+----------+--------+------+---------------------+---------------+");
		printf("| %-9s| %-7s| %-5s| %-20s| %-13s |\n",
				"BILL-ID", "ITEMS", "QTY", "CUSTOMER", "PRICE(+TAX)");
		puts("+----------+--------+------+---------------------+---------------+");
		k = 0;
		while(k<hbill->ordercount) {
			order = &hbill->order[k];
			//if(strlen(order->cust.name) > 0) {
				/*sscanf(line, "%d, %s, %lf, %lf, %s, %s, %s, %d, %c, %s",
						&orderid,
						itemname,
						&itemprice,
						&itemweight,
						custname,
						custemail,
						custaddr,
						&custage,
						&custsex,
						timestamp);*/
				orderid = atoi(order->id);
				if(orderid>-1) {
					strcpy(itemname, "TBA");
					//itemprice = order->item[0].sprice;
//					itemqty = order->itemqty;
					/** Loop the items for price */
//					itemcnt = order->itemcount;
					i = 0;
					otot = 0.0;
					if(itemcnt>0) {
/*						while(i<itemcnt) {
							item = &order->item[i];
							itemid = atoi(item->id);
							if(itemid>-1) {
								itemoqty = item->quantity;
								item = &hbill->invt.item[itemid-1];
								otot += item->sprice*itemoqty;
							}
							++i;
						}
*/						otot += servicetax + vattax;
						totprice += otot;
					}
//					itemweight = order->item[0].weight;
					strcpy(custname, order->cust.name);
					strcpy(custemail, order->cust.email);
					strcpy(timestamp, order->timestamp);

					printf("| %-9d| %-7d| %-5d| %-20s| %13.2lf |\n", orderid, itemcnt,
												itemqty,
												//itemprice,
												//itemweight,
												custname,
												//custemail,
												otot);
					ordercount += 1;
				//}
			}
			++k;
		}
		puts("+----------+--------+------+---------------------+---------------+");
		/*totprice += servicetax + vattax;
		printf("\t\t\t\t\t\tService tax:\t\t%.2lf\n", servicetax);
		printf("\t\t\t\t\t\tVAT tax:\t\t%.2lf\n", vattax);*/
		printf("| %-9s| %-7s| %-5s| %-20s| %13.2lf |\n", 
				"", "", "", "Total", totprice);
		puts("+----------+--------+------+---------------------+---------------+");
	}

	if(ordercount == 0) {
		puts("No bills created..");
	}
}

int save_orderid(HBll *hbill) {
	int orderid = FIRSTORDERID-1;
	int ordercnt = 0;
	time_t ticks;
	ORD *order;
	FILE *fp;
	char filename[MAX_FILEPATH];

	/** Params */
	char itemname[MAX_INPUT];
	double itemprice = 0.0;
	int itemqty = 0;
	double itemwt = 0.0;
	char custname[MAX_INPUT];
	char custemail[MAX_INPUT];
	char custaddr[MAX_STR];
	int custage = 0;
	char custsex = 'm';

	strcpy(filename, ORDERSFILE);
	ordercnt = hbill->ordercount;
	orderid += hbill->ordercount;
	++orderid; // The order id is not an array index, it starts from 1
	hbill->curorderid = orderid;
	order = &hbill->order[ordercnt];
	sprintf(order->id, "%d", orderid);

	// Save timestamp
	ticks = time(NULL);
	sprintf(order->timestamp, "%s", ctime(&ticks));
	strcpy(itemname, "TBA");
	itemqty = 0;
	strcpy(custname, "TBA");
	strcpy(custemail, "TBA");
	strcpy(custaddr, "TBA");

	// Save to file
	fp = fopen(filename, "a+");
	if(fp) {
		fprintf(fp, "%d, %s, %lf, %d, %lf, %s, %s, %s, %d, %c, %.24s\n",
				orderid,
				itemname,
				itemprice,
				itemqty,
				itemwt,
				custname,
				custemail,
				custaddr,
				custage,
				custsex,
				ctime(&ticks));

		sprintf(order->id, "%d", orderid);
/*		strcpy(order->item[0].name, itemname);
		order->item[0].sprice = itemprice;
		order->item[0].quantity = itemqty;
		order->item[0].weight = itemwt;
*/		strcpy(order->cust.name, custname);
		strcpy(order->cust.email, custemail);
		strcpy(order->cust.address, custaddr);
		order->cust.age = custage;
		order->cust.sex = custsex;

		fclose(fp);
		++hbill->ordercount; // This increment is for show-orders function that is always 1 extra
		return TRUE;
	}
	return FALSE;
}

void save_custname(HBll *hbill, char *name) {
	// save customer name
	// Should be ideally run right after creating new order
	// Get the order using fetch_order() function
	//fetch_order_(hbill, hbill->curorderid);

	// Fetch from memory struct's data and save_orders_() again to file
	ORD *order = &hbill->order[hbill->curorderid];
	order->cust.name[0] = '\0';
	strip_newline(name, MAX_INPUT);
	strcpy(order->cust.name, name);
	//save_orders_(hbill);
}

void save_custemail(HBll *hbill, char *email) {
	// save customer email

	//fetch_order_(hbill, hbill->curorderid);
	ORD *order = &hbill->order[hbill->curorderid];
	strip_newline(email, MAX_INPUT);
	strcpy(order->cust.email, email);
	//save_orders_(hbill);
}

void save_custaddr(HBll *hbill, char *addr) {
	// customer address
	ORD *order = &hbill->order[hbill->curorderid];
	strip_newline(addr, MAX_STR);
	strcpy(order->cust.address, addr);
	//save_orders_(hbill);
}

void save_custsex(HBll *hbill, char sex) {
	// customer sex
	ORD *order = &hbill->order[hbill->curorderid];
	order->cust.sex = sex;
	//save_orders_(hbill);
}

void save_custage(HBll *hbill, int age) {
	// customer age
	ORD *order = &hbill->order[hbill->curorderid];
	order->cust.age = age;
}

int save_order(HBll *hbill) {
	FILE *fp; // File handle for storing data in the flat file
	char filename[] = "hbill-orders.csv";
	ORD *curorder = &hbill->order[hbill->curorderid];
	int orderid;// = hbill->ordercount+1;
	char custname[MAX_INPUT];
	char custaddr[MAX_STR];
	char custemail[MAX_INPUT];
	int custage;
	char custsex;

	char itemname[MAX_INPUT];
	double itemwt;
	int itemqty;
	int itemcount;
	int success;

	time_t ticks;

	success = FALSE;
	/** Assuming current order details and customer details are stored in their structures */
	/** Use much simpler variable names for clarity */
	strcpy(custname, curorder->cust.name);
	strcpy(custaddr, curorder->cust.address);
	strcpy(custemail, curorder->cust.email);
	custage = curorder->cust.age;
	custsex = curorder->cust.sex;
	itemcount = curorder->itemcount;
/*	itemqty = curorder->item[itemcount].quantity;
	strcpy(itemname, curorder->item[itemcount].name);
	itemwt = curorder->item[itemcount].weight;
*/	orderid = hbill->ordercount;
	ticks = time(NULL);

	fp = fopen(filename, "a+"); // Append the file to allow more records
	if(fp) {
/*		fprintf(fp, "%d, %s, %d, %f, %s, %s, %s, %d, %c, %.24s\n",
				orderid,
				itemname,
				itemqty,
				itemwt,
				custname,
				custemail,
				custaddr,
				custage,
				custsex,
				ctime(&ticks));
		fclose(fp);
		++hbill->ordercount;
		success = TRUE;
*/	}

	return success;
}

void del_order_(HBll *hbill) {
	int itemid;
	int orderid;
	int i = 0, j = 0, k = 0;
	int itemcnt = 0, itemqty = 0;
	ORD *order;
	ITM *item;
	char filename[MAX_FILEPATH];
	// Get items of order - reset each of them in inventory with their quantities
	orderid = hbill->curorderid; // Already changed to get index
	order = &hbill->order[orderid];
	itemcnt = order->itemcount;
	if(itemcnt>0) {
/*		for(i=0; i<itemcnt; i++) {
			item = &order->item[i];
			itemqty = 0;
			itemqty += item->quantity;
			itemid = atoi(item->id);
			strcpy(item->id, "-1"); // Unset item in order

			for(k=0; k<hbill->invt.itemcount; k++) {
				item = &hbill->invt.item[k];
				if(itemid == atoi(item->id)) { // Found in inventory
					item->quantity += itemqty; // Restore in inventory
					break; // Found only once
				}
			}
		}
*/	}

	// Remove/Unlink the file associated with it
	sprintf(filename, "%s%c%s", order->id, '.', "csv");
	if(file_exists_(filename)) unlink(filename);
	
	// Unset the key by clearing any data, when orders file is saved we don't add that order
	strcpy(order->id, "-1"); // Unset order

}

int ord_key_exists_(HBll *hbill, int key) {
	int orderid = 0;
	ORD *order;
	int i = 0;
	int exists = FALSE;
	int ordercnt = hbill->ordercount;
	if(ordercnt > 0 && key <= ordercnt && key > -1) { // key <= because key is supplied by user
		//--key;
		for(i=0; i<ordercnt; i++) {
			if(key == i) {
				exists = TRUE; break;
			}
		}
	}
	return exists;
}

/** Single order file read */
void scan_oline_(HBll *hbill, char line[], int lines) {
	// Fetch items from line using strtok()
	int i = 0, j = 0, k = 0;
	char delim[] = ",";
	char filename[MAX_FILEPATH];

	int oid = 0;
	char itemname[MAX_INPUT];
	double itemprice = 0.0;
	int itemqty = 0;
	double itemwt = 0.0;
	char custname[MAX_INPUT];
	char custemail[MAX_INPUT];
	char custaddr[MAX_STR];
	int custage = 0;
	char custsex = 'm';
	char timestamp[MAX_INPUT];
	time_t ticks;
	char nline[MAX_LINE];
	FILE *fp;

	ORD *order;
	ITM *item;

	char *token;
	if(strlen(line) > 0) {
		token = strtok(line, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			oid = atoi(trim(token, MAX_INPUT));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			strcpy(itemname, trim(token, MAX_INPUT)); // We are calling trim too many => buffer overflow?
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			itemprice = atof(trim(token, MAX_INPUT));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			itemqty = atoi(trim(token, MAX_INPUT));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			itemwt = atof(trim(token, MAX_INPUT));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			strcpy(custname, trim(token, MAX_INPUT));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			strcpy(custemail, trim(token, MAX_INPUT));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_STR);
			strcpy(custaddr, trim(token, MAX_STR));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, 3);
			custage = atoi(trim(token, 3));
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, 2);
			custsex =  trim(token, 2)[0];
		}
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			strcpy(timestamp, trim(token, MAX_INPUT));
		}
	}

	/** Fill up the struct data */
//	order = &hbill->order[lines];
	order = hbill->order; // First item
	sprintf(order->id, "%d", oid);
	// Try fetching all items for that order
	filename[0] = '\0';
	sprintf(filename, "%s.csv", order->id);
	fp = fopen(filename, "r");
	i = 0;
	if(fp) {
/*		while((fgets(nline, MAX_LINE, fp)) != NULL) {
//			item = &order->item[i];
			item = &order->item; // First one
			token = strtok(nline, ",");
			if(token != NULL) {
				token = strtok(NULL, ",");
				if(token != NULL) {
					strcpy(item->oitemid, trim(token, MAX_INPUT));
					token = strtok(NULL, ",");
/*					if(token != NULL) {
						strcpy(item->name, trim(token, MAX_INPUT));
						token = strtok(NULL, ",");
						if(token != NULL) {
							itemqty = atoi(trim(token, MAX_INPUT));
							item->oitemqty = itemqty;
//							order->itemqty += itemqty;
						}
//					}
*//*				}
			}
			++order->itemcount; ++i;
			if(!order->item) {
				order->item = (ITM *) malloc(sizeof(ITM)); // Move the current order item in list (or) just create memory for next in list
		}

*/		fclose(fp);
	} else
		order->itemcount = 0;
	/*strcpy(order->item[0].name, itemname);
	order->item[0].sprice = itemprice;
	order->item[0].quantity = itemqty;
	order->item[0].weight = itemwt;*/
	//order->itemcount = itemcount;
	strcpy(order->cust.name, custname);
	strcpy(order->cust.email, custemail);
	strcpy(order->cust.address, custaddr);
	order->cust.age = custage;
	order->cust.sex = custsex;
	strcpy(order->timestamp, timestamp);
	hbill->curorderid = oid; // The last fetched*/
}

void fetch_order_(HBll *hbill, int orderid) {
	// Load up file and work through all lines and stop at orderid, fetch the complete line and propagate the struct
	FILE *fp;
	int oid;
	ORD *order;
	char filename[] = "hbill-orders.csv";
	char line[MAX_LINE];
	char itemname[MAX_INPUT];
	double itemprice;
	int itemqty;
	double itemwt;
	char custname[MAX_INPUT];
	char custemail[MAX_INPUT];
	char custaddr[MAX_INPUT];
	int custage;
	char custsex;
	time_t ticks;
	char timestamp[MAX_INPUT];

	int orderfound = FALSE;
	order = &hbill->order[hbill->curorderid];
	fp = fopen(filename, "r");
	if(fp) {
		while(fgets(line, MAX_LINE, fp)) {
			sscanf(line, "%d, %s, %lf, %d, %lf, %s, %s, %s, %d, %c, %s",
					&oid,
					itemname, // Items can be colon separated
					&itemprice,
					&itemqty, // Items qty can be colon separated
					&itemwt,
					custname,
					custemail,
					custaddr,
					&custage,
					&custsex,
					timestamp);

			/** Now tally the iod to orderid sent from argument and pop the order */
			/** After popping the order, break the loop and propagate the structs appropriately */

			if(oid == orderid) { // Found match
				orderfound = TRUE;
				break;
			}
		}

		if(orderfound == TRUE) {
			// Loop order items

			strcpy(order->cust.name, custname);
			strcpy(order->cust.email, custemail);
			strcpy(order->cust.address, custaddr);
			order->cust.age = custage;
			order->cust.sex = custsex;
			strcpy(order->timestamp, timestamp);
		}

		fclose(fp);
	}
}

/*void print_order_(HBll *hbill) {
	char filename[MAX_FILEPATH];
	unsigned int rando;
	char cmd[MAX_STR];
	time_t ticks;
	int orderid = 0;
	ORD *order;

	ticks = time(NULL);
	srand(ticks);
	rando = rand();

	sprintf(filename, "%s%c%s", TMPDIR, DS, rando);
	orderid = HBll->curorderid-1; // Index
	
	sprintf(cmd, "lpr %s", filename);
	system(cmd);

	if(file_exists_(filename)) unlink(filename);
}*/ /** Moved to main source file */

void reset_order(HBll *hbill) {
	/** Clear the data in the structures and pointers */
}

int save_orders_(HBll *hbill) {
	/** Function to remove old orders file and save using data from memory */
	FILE *fp;
	int orderid;
	int k = 0;
	ORD *order;
	int end = FALSE;
	int saved = FALSE;
	if(file_exists_(ORDERSFILE) == TRUE)
		unlink(ORDERSFILE);

	fp = fopen(ORDERSFILE, "w");
	if(fp) {
		for(k=0; k<hbill->ordercount; k++) {
			order = &hbill->order[k];
			orderid = atoi(order->id);
			if(orderid>-1) {
				fprintf(fp, "%d, %s, %.2lf, %d, %.2lf, %s, %s, %s, %d, %c, %s\n",
					orderid,
//					order->item[0].name,
//					order->item[0].sprice,
//					order->itemqty,
//					order->item[0].weight,
					order->cust.name,
					order->cust.email,
					order->cust.address,
					order->cust.age,
					order->cust.sex,
					order->timestamp);
			}
		}
		saved = TRUE;
		fclose(fp);
	}
	return saved;
}