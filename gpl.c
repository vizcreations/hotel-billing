/**
* @gpl license function source for hotel billing solution
* @author VizCreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hbll.h"

void show_license() {
	char gpl[] = "HBS - Hotel Billing System  Copyright (C) 2014  VizCreations\n" 
			"This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\n"
			"This is free software, and you are welcome to redistribute it\n"
			"under certain conditions; type `show c' for details.";
	puts(gpl);
}