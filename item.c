/**
* @item source file with functions for hotel-billing
* @author vizcreations
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include "hbll.h"

int item_qty_overflow_(HBll *hbill, int qty) {
	int itemid = 0;
	int overflow;
	int itemqty = 0;
	ITM *item;
	overflow = FALSE;

	itemid = hbill->invt.curitemid;
	item = &hbill->invt.item[itemid-1]; // Inventory item

	itemqty = item->quantity;
	if((itemqty - qty) < 0)
		overflow = TRUE;

	return overflow;
}

void save_item_(HBll *hbill, int qty) { // Save an item to bill order
	int orderid = 0;
	int itemcount = 0;
	char filename[MAX_FILEPATH];
	FILE *fp;
	int itemid = 0;
	char itemname[MAX_INPUT];
	int invitemid = 0;
	int itmid = 0;
	ORD *order;
	time_t ticks;

	if(item_qty_overflow_(hbill, qty) == TRUE) {
		puts("Stock overflow / Stockout..");
		exit(0);
	}
	order = &hbill->order[hbill->curorderid]; // curorderid is attained from input/select
	//strcpy(filename, name);
	itemid = hbill->invt.curitemid; // Still the user's selected number, not the array index
	filename[0] = '\0';
	orderid = hbill->curorderid;
	orderid += FIRSTORDERID; // To counter the minus done to support the array indices
	sprintf(filename, "%d", orderid);
	strcat(filename, ".csv");
	itemcount = itmid = order->itemcount;
	itmid += FIRSTOITEMID-1; ++itmid;
//	order->curitemid = itmid;

	// Set up order item details
//	strcpy(order->item->oitemid, hbill->invt.item.id); // Copy id from inventory to order
	order->oitem->oitemid = atoi(hbill->invt.item->id); // Current item in list
//	strcpy(order->item[itemcount].name, hbill->invt.item[itemid-1].name); // Copy name from inventory to order
//	strcpy(itemname, order->item[itemcount].name);
//	order->item[itemcount].quantity = qty;
//	order->itemqty += qty;

	/** Reduce in the inventory */
	hbill->invt.item->quantity -= qty;
	ticks = time(NULL);
	// Save to orders file
	fp = fopen(filename, "a+"); // Append to existing file or create new file
	if(fp) {
		fprintf(fp, "%d, %d, %s, %d, %.24s\n",
				itmid,
				itemid,
				itemname,
				qty,
				ctime(&ticks)); // Coming soon.
		fclose(fp);
		++order->itemcount;
	}

	// Point to next item
	if(!hbill->order->oitem->next)
		hbill->order->oitem->next = (OITEM *) malloc(sizeof(ITM));
/*
	if(!hbill->invt.item)
		hbill->invt.item = (ITM) malloc(sizeof(ITM));
*/	save_items_(hbill);
}

void edit_oitem_(HBll *hbill, char *name, int tot) {
	int i = 0;
	int orderid = 0;
	int itemid = 0;
	ORD *order;
	ITM *item;

	order = &hbill->order[hbill->curorderid];
}
/*
void save_item_price_(HBll *hbill, char *price) {
	int itemcount = 0;
	ORD *order = &hbill->order[hbill->curorderid];
	itemcount = order->itemcount;
	order->item[itemcount].sprice = atof(trim(price, MAX_INPUT));
}

void save_item_wt_(HBll *hbill, char *wt) {
	int itemcount = 0;
	ORD *order = &hbill->order[hbill->curorderid];
	itemcount = order->itemcount;
	order->item[itemcount].weight = atof(trim(wt, MAX_INPUT));
}
*/
void save_item_qty_(HBll *hbill, int cnt) {
	int itmid = 0;
	int itemid = 0; // Inventory item-id
	int diff = 0;
	int qty = 0;
	ORD *order;
	order = &hbill->order[hbill->curorderid];
//	itmid = order->curitemid; // This is array index of order
	itmid = order->oitem->oitemid;
//	qty = order->item[itmid].quantity;
	qty = order->oitem->oitemqty;
	itemid = order->oitem->oitemid; // Id of inventory item
	if(qty<cnt) { // Added more to order
		diff = cnt-qty;
		if(item_qty_overflow_(hbill, diff) == TRUE) {
			puts("Stock overflow / Stockout.."); exit(0);
		}
//		order->itemqty += diff;
		diff = -diff; // Reduce from inventory
	}
	else if(qty>cnt) { // Reduced from order
		diff = qty-cnt; // Add to inventory
//		order->itemqty -= diff; // Reduce from order
		//diff = -diff; // For inventory
	}
	//printf("ITEMID: %d; ITMID: %d; QTY: %d; CNT: %d; DIFF: %d\n", itemid, itmid, order->item[itmid].quantity, cnt, diff);
	/** Update in order */
	order->oitem->oitemqty = cnt;

	/** Reduce/Increase in the inventory */
	//printf("DIFF: %d\n", diff);
	hbill->invt.item->quantity += diff;
}

int save_oitems_(HBll *hbill) {
	int i = 0;
	int orderid = 0;
	int itemid = 0;
	char itemname[MAX_INPUT];
	int qty = 0;
	FILE *fp;
	char filename[MAX_FILEPATH];
	int itemcount = 0;
	ITM *item;
	ORD *order;
	time_t ticks;

	orderid = hbill->curorderid;
	ticks = time(NULL);

	order = &hbill->order[orderid];
	sprintf(filename, "%s.csv", order->id);
	if(file_exists_(filename)) unlink(filename);
	fp = fopen(filename, "w+"); // Fresh file
	if(fp) {
		itemcount = order->itemcount;
/*		for(i=0; i<itemcount; i++) {
			item = order->oitem; // Already address
			itemid = order->oitem->oitemid;
			if(itemid>-1) {
				strcpy(itemname, item->name);
				qty = item->quantity;
				fprintf(fp, "%d, %d, %s, %d, %.24s\n",
						i+FIRSTOITEMID,
						itemid,
						itemname,
						qty,
						ctime(&ticks));
			}
		}
*/		fclose(fp);
		return TRUE;
	}

	return FALSE;
}

void add_item_(HBll *hbill, char *name, int cnt) {
	FILE *fp;
	int itemid = 0;
	int itemcount = 0;
	char itemname[MAX_INPUT];
	double itemsprice = 0.0;
	double itemoprice = 0.0;
	double itemmrp = 0.0;
	double itemwt = 0.0;
	int itemqty = 0;
	ITM *item;
	time_t ticks;

	itemcount = hbill->invt.itemcount;
	itemid = itemcount;

	/** Get ID from last item in inventory */
	item = &hbill->invt.item[itemcount-1]; // Final item
	itemid = atoi(item->id);
	itemqty = cnt;
	++itemid;

	ticks = time(NULL);
	strip_newline(name, MAX_INPUT);
	strcpy(itemname, name);
	fp = fopen(ITEMSFILE, "a+"); // Append to file
	if(fp) {
		fprintf(fp, "%d, %s, %.2lf, %.2lf, %.2lf, %.2lf, %d\n",
				itemid,
				itemname,
				itemsprice,
				itemoprice,
				itemmrp,
				itemwt,
				itemqty,
				ctime(&ticks));
		fclose(fp);
		item = &hbill->invt.item[itemcount];
		sprintf(item->id, "%d", itemid);
		strcpy(item->name, itemname);
		item->sprice = itemsprice;
		item->oprice = itemoprice;
		item->mrp = itemmrp;
		item->quantity = itemqty;
		item->weight = itemwt;
		++hbill->invt.itemcount; // Always increases for next, and used as max-flag for show-items().
	}

}

void edit_item_(HBll *hbill, int id, char *newname, int tot) {
	char itemid[MAX_INPUT];
	char itemname[MAX_INPUT];
	int itemcount = 0;
	int found = FALSE;
	int i = 0;
	int curitem = -1;
	ITM *item;
	itemcount = hbill->invt.itemcount;
	strip_newline(newname, MAX_INPUT);
	strcpy(itemname, newname);
	sprintf(itemid, "%d", id);
	/*for(i=0; i<itemcount; i++) {
		item = &hbill->invt.item[i];
		if(strcmp(itemid, item->id)) {
			found = TRUE; break;
			curitem = i;
		}
	}*/
	if(id>0) found = TRUE;
	if(found == TRUE && id > -1) {
		item = &hbill->invt.item[id-1]; // In index, the ID is minus one because it's an array.
		strcpy(item->name, newname); // Save to structure
		item->quantity = tot;
	}
}

void edit_item_qty_(HBll *hbill, int id, int tot) {
	char itemid[MAX_INPUT];
	int i = 0;
	ITM *item;
	int itemcount;
	itemcount = hbill->invt.itemcount;
	if(id > -1) {
		item = &hbill->invt.item[id-1];
		item->quantity = tot;
	}
}

void edit_item_price_(HBll *hbill, int id, double price) {
	ITM *item;
	int itemcount = 0;
	if(id > -1) {
		item = &hbill->invt.item[id-1]; // -1 for index of array
		item->sprice = price;
	}
}

void edit_item_weight_(HBll *hbill, int id, double weight) {
	ITM *item;
	int itemcount = 0;
	if(id > -1) {
		item = &hbill->invt.item[id-1];
		item->weight = weight;
	}
}

int save_items_(HBll *hbill) {
	FILE *fp;
	int itemcount = 0;
	int i = 0;
	ITM *item;
	int itemid = 0;
	char itemname[MAX_INPUT];
	double itemsprice = 0.0;
	double itemoprice = 0.0;
	double itemmrp = 0.0;
	double itemwt = 0.0;
	int itemqty = 0;

	int saved_items;
	saved_items = FALSE;
	itemcount = hbill->invt.itemcount;
	if(file_exists_(ITEMSFILE)) unlink(ITEMSFILE);

	fp = fopen(ITEMSFILE, "w+");
	if(fp) {
		for(i=0; i<itemcount; i++) {
			item  = &hbill->invt.item[i];
			itemid = atoi(item->id);
			if(itemid>0) {
				strcpy(itemname, item->name);
				itemsprice = item->sprice;
				itemoprice = item->oprice;
				itemmrp = item->mrp;
				itemwt = item->weight;
				itemqty = item->quantity;

				fprintf(fp, "%d, %s, %.2lf, %.2lf, %.2lf, %.2lf, %d\n",
						itemid,
						itemname,
						itemsprice,
						itemoprice,
						itemmrp,
						itemwt,
						itemqty);
			}
		}
		if(i>0) saved_items = TRUE;
		fclose(fp);
	}
	return saved_items;
}

int del_item_(HBll *hbill) {
	int itemid;
	ITM *item;
	int deleted;
	int itemcnt = 0, i = 0, id = 0;
	itemcnt = hbill->invt.itemcount;
	id = hbill->invt.curitemid;
	deleted = FALSE;

	// Remove key of that item, basically clearing data
	for(i=0; i<itemcnt; i++) {
		item = &hbill->invt.item[i];
		itemid = atoi(item->id);
		if(id==itemid) {
			strcpy(item->id, "-1"); // Clearing for save-items function
			deleted = TRUE;
			break;
		}
	}

	// When saving inventory, simply don't include the key
	return deleted;
}

void del_oitem_(HBll *hbill) {
	int itemid, itemcnt;
	int orderid, ordercnt;
	int i = 0, j = 0, k = 0;

	ORD *order;
	ITM *item;
	int itemqty = 0;

	itemid = hbill->invt.curitemid; // The ID of item selected
	ordercnt = hbill->ordercount;
	// Get item key from order items array and unset it
/*	for(i=0; i<ordercnt; i++) {
		order = &hbill->order[i];
		itemcnt = order->itemcount;
		for(j=0; j<itemcnt; j++) { // Loop through order items..
//			item = order->item;
			itemqty = 0;
			if(itemid == atoi(item->id)) { // Found item
				itemqty += item->quantity; // Get quantity to restore in inventory
				strcpy(item->id, "-1"); // Unset in order

				// Reset the inventory with item and it's quantity
				// Loop within inventory to find item and restore
				for(k=0; k<hbill->invt.itemcount; k++) {
					item = &hbill->invt.item[k];
					if(itemid==atoi(item->id)) {
						item->quantity += itemqty; // Restore inventory item quantity
						break;
					}
				} // Occurs only once, so break when found and reduce overhead..
				order->itemcount--; // Reduce 1 item
//				order->itemqty -= itemqty; // Reduce order total item qty
			}
		}
	}
*/
}

int itm_key_exists_(HBll *hbill, int key) {
	int itemid = 0;
	int exists = FALSE;
	int i = 0;
	int itemcnt = 0;
	int orderid = hbill->curorderid;
	//ORD *order = hbill->order[orderid];
	//int itemcnt = order->itemcount;
	itemcnt = hbill->invt.itemcount;
	if(itemcnt>0 && key<=itemcnt && key>0) {
		--key; // To match array index
		for(i=0; i<itemcnt; i++) {
			if(key == i) {
				exists = TRUE; break;
			}
		}
	}
	return exists;
}

int itm_ord_key_exists_(HBll *hbill, int key) {
	int itemid = 0;
	int orderid = 0;
	int itemcnt;
	int i = 0;
	ORD *order;
	int exists = FALSE;
	//key = key-10000; // Items of order start ID from 10000
	orderid = hbill->curorderid;
	order = &hbill->order[orderid];
	itemcnt = order->itemcount;
	if(itemcnt>0 && key<=itemcnt && key>-1) {
		//--key;
		for(i=0; i<itemcnt; i++) {
			if(key == i) {
				exists = TRUE; break;
			}
		}
	}
	return exists;
}

int itm_ords_key_exists_(HBll *hbill, int key) {
	int itemid = 0;
	int orderid = 0;
	int itemcnt;
	int ordercnt;
	int i=0, j=0;

	ORD *order;
	ITM *item;
	int exists = TRUE;
	int found = FALSE;
	ordercnt = hbill->ordercount; // Last order doesn't exist
/*
	while(i<ordercnt) {
		order = &hbill->order[i];
		itemcnt = order->itemcount;
		if(itemcnt>0 && key>-1) {
			for(j=0; j<itemcnt; j++) {
//				item = order->item;
				if(key == atoi(item->id)) {
					exists = TRUE;
					found = TRUE;
					break;
				}
			}
		}
		++i;
	}
*/	if(found == FALSE) exists = FALSE;
	return exists;
}

void scan_olineitem(HBll *hbill, char *line, int counter) {
	int i = 0;
	int itemid = 0;
	char itemname[MAX_INPUT];
	double itemsprice = 0.0;
	double itemoprice = 0.0;
	double itemmrp = 0.0;
	double itemwt = 0.0;
	int itemqty = 0;
	char timestamp[MAX_INPUT];

	ITM *item;

	char *token; // To pull out data from line
	if(strlen(line) > 0) { // Proceed
		token = strtok(line, ",");
		if(token != NULL) itemid = atoi(trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) strcpy(itemname, trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) itemsprice = atof(trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) itemoprice = atof(trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) itemmrp = atof(trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) itemwt = atof(trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) itemqty = atoi(trim(token, MAX_INPUT));
		token = strtok(NULL, ",");
		if(token != NULL) {
			strip_newline(token, MAX_INPUT);
			strcpy(timestamp, token);
		}
	}

	// Create item in memory
//	item = &hbill->invt.item[counter];
	item = hbill->invt.item; // Already a pointer
	sprintf(item->id, "%d", itemid);
	strcpy(item->name, itemname);
	item->sprice = itemsprice;
	item->oprice = itemoprice;
	item->mrp = itemmrp;
	item->weight = itemwt;
	item->quantity = itemqty;

	// Setup in current item struct and initialize next one
	if(!hbill->invt.item->next) {
		hbill->invt.item->next = (ITM *) malloc(sizeof(ITM));
		hbill->invt.item = hbill->invt.item->next; // All pointers assignment :-)
	}
	// Timestamp add ?
}

void show_items_(HBll *hbill) {
	int i = 0;
	int itemcount = 0;
	int itemid = 0;
	char itemname[MAX_INPUT];
	double itemsprice = 0.0;
	double itemoprice = 0.0;
	double itemmrp = 0.0;
	double itemwt = 0.0;
	double tot = 0.0;
	int itemqty = 0;
	ITM *item;
	itemcount = hbill->invt.itemcount;

	if(itemcount > 0) {
		puts("+-----+---------------------+-----------+----------+----------+-----------+");
		printf("| %-4s| %-20s| %-10s| %-9s| %-9s| %-9s |\n", 
				"ID", "ITEM NAME", "QUANTITY", "MRP", "WEIGHT", "PRICE");
		puts("+-----+---------------------+-----------+----------+----------+-----------+");
		for(i=0; i<itemcount; i++) {
			item = &hbill->invt.item[i];
			itemid = atoi(item->id);
			strcpy(itemname, item->name);
			itemsprice = item->sprice;
			itemoprice = item->oprice;
			itemmrp = item->mrp;
			itemwt = item->weight;
			itemqty = item->quantity;
			tot += itemsprice*itemqty;

			printf("| %-4d| %-20s| %-10d| %-9.2lf| %-9.2lf| %9.2lf |\n",
					itemid,
					itemname,
					itemqty,
					//itemoprice,
					itemmrp,
					itemwt,
					itemsprice
			);
		}
		puts("+-----+---------------------+-----------+----------+----------+-----------+");
		printf("| %-4s| %-20s| %-10s| %-9s| %-9s| %9.2f |\n",
			"", "", "", "", "Total", tot);
		puts("+-----+---------------------+-----------+----------+----------+-----------+");
	} else puts("Inventory is empty..");
}