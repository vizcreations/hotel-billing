/**
* @hotel-billing solution for a hotel/restaurant
* @author vizcreations
* @target-interface command-line/console/shell
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include "hbll.h"

int main(int argc, char *argv[], char *env[]) {
	HBll hbill;
	int success;
	char op = 'o';
	char *val; // Value
	int tot; // No. of items
	int id;
	int i = 0;
	char sel;
	int opfound;
	ORD *order;
	ITM *item;
	opfound = success = FALSE;

	show_license();
	init__(&hbill);
	if(argc<2) {
		printf("Usage: %s [-option] [name] [value]\n", argv[0]);
		return -1;
	}

	if(SHOW_ENV == TRUE) {
		i = 0;
		while(env[i++]) {
			printf("ENV[%d]: %s\n", i, env[i]);
		}
	}

	if(SHOW_ARGV == TRUE) {
		i = 0;
		while(i++<argc) {
			printf("ARGV[%d]: %s\n", i, argv[i]);
		}
	}

	if(argv[1][0] == '-') { // Giving option
		op = argv[1][1];

		switch(op) {
			case 'n': // New record
				{
					success = save_orderid(&hbill); // Create new record in file and set cur-orderid
					if(success == FALSE)
						puts("Technical problem encountered. Couldn't create order!\n");
					else
						puts("Order saved successfully!\n");

					show_orders_(&hbill);
				}
				break;
			case 'u': // Update record
				{
					if(argc > 2) {
						// Compare third argument
						if(!isdigit(argv[2][0])) {
							puts("Unknown option!\n");
							return -2;
						}
						hbill.curorderid = (atoi(argv[2])) - FIRSTORDERID; // Set current order ID whose data we alter
						order = &hbill.order[hbill.curorderid];
						if(ord_key_exists_(&hbill, hbill.curorderid) == FALSE) {
							puts("Out of bound order id supplied!\n"); exit(0);
						}
						if(argc > 4) {
							// Compare fourth argument
							if(strlen(argv[4]) > 0) {
								val = argv[4];

								if(strcmp(argv[3], "cn") == 0) // Customer name update
									save_custname(&hbill, val);
								else if(strcmp(argv[3], "ce") == 0)
									save_custemail(&hbill, val);
								else if(strcmp(argv[3], "ca") == 0)
									save_custage(&hbill, atoi(val));
								else if(strcmp(argv[3], "cs") == 0)
									save_custsex(&hbill, atoi(val));
								else if(strcmp(argv[3], "ad") == 0) // Customer address
									save_custaddr(&hbill, val);

								else if(strcmp(argv[3], "ia") == 0) { // Item add name, qty
									/** Decide which item from inventory */
									id = atoi(val); // ID starts from 1
									//--id; // For index
									if(itm_key_exists_(&hbill, id) == FALSE) {
										puts("Out of bound item id supplied!\n");
										exit(0);
									}
									hbill.invt.curitemid = id;
									tot = 1;
									if(argc>5) {
										tot = atoi(argv[5]);
										save_item_(&hbill, tot);
									}
								} else if(strcmp(argv[3], "iq") == 0) { // Item quantity
									id = atoi(val);
									id -= FIRSTOITEMID; // Set index
									if(itm_ord_key_exists_(&hbill, id) == FALSE) {
										puts("Out of bound item id supplied!\n");
										exit(0);
									}

									/** Set inventory current item-id */
//									hbill.invt.curitemid = atoi(hbill.order[hbill.curorderid].item[id].id);
									/** Set current order current item-id */
//									hbill.order[hbill.curorderid].curitemid = id;
									if(argc>5) {
										tot = 0;
										tot = atoi(argv[5]);
										save_item_qty_(&hbill, tot); // If val == "no", do not update name
										save_oitems_(&hbill);
										save_items_(&hbill);
									}
								}/* else if(strcmp(argv[3], "ip") == 0) { // Item price
									id = atoi(val);
									hbill.invt.curitemid = id;
									if(argc>5) {
										val = argv[5];
										save_item_price_(&hbill, val);
									}
								} else if(strcmp(argv[3], "iw") == 0) { // Item weight
									id = atoi(val);
									hbill.invt.curitemid = id;
									if(argc>5) {
										val = argv[5];
										save_item_wt_(&hbill, val);
									}
								}*/ else if(strcmp(argv[3], "ir") == 0) { // Item delete
									id = atoi(val);
									id -= FIRSTOITEMID; // Set index
									if(itm_ord_key_exists_(&hbill, id) == FALSE) {
										puts("Out of bound item id supplied!\n");
										exit(0);
									}

									hbill.invt.curitemid = order->oitem->oitemid;
//									order->curitemid = id;
									del_oitem_(&hbill);
									save_oitems_(&hbill);
									save_items_(&hbill);
								}

								save_orders_(&hbill);
							}
						}
					}
					show_orders_(&hbill);
				}
				break;
			case 'v': // View selected record -> updated cur-Orderid to selected order
				{
					if(argc > 2) {
						// Compare third argument
						if(!isdigit(argv[2][0])) {
							puts("Unknown option!\n");
							return -2;
						}
						hbill.curorderid = (atoi(argv[2])) - FIRSTORDERID; // Set current order ID whose data we alter
						if(ord_key_exists_(&hbill, hbill.curorderid) == FALSE) {
							puts("Out of bound order id supplied!\n"); exit(0);
						}
						show_order_(&hbill);
					}
				}
				break;
			case 'p': // Print
				{
					if(argc > 2) {
						if(!isdigit(argv[2][0])) {
							puts("Unknown option!\n");
							return -2;
						}
						hbill.curorderid = (atoi(argv[2])) - FIRSTORDERID;
						if(ord_key_exists_(&hbill, hbill.curorderid) == FALSE) {
							puts("Out of bound order id supplied!\n"); exit(0);
						}
						print_order_(&hbill, FALSE);
					}
				}
				break;
			case 's': // Save to file
				{
					if(argc > 2) {
						if(!isdigit(argv[2][0])) {
							puts("Unknown option!\n");
							return -2;
						}
						hbill.curorderid = (atoi(argv[2]))-FIRSTORDERID;
						if(ord_key_exists_(&hbill, hbill.curorderid) == FALSE) {
							puts("Out of bound order id supplied!\n"); exit(0);
						}
						print_order_(&hbill, TRUE);
					}
				}
				break;
			case 'l': // List/Show records
				{
					show_orders_(&hbill);
				}
				break;
			case 'd': // Delete record -> Deletes record with id of next argument
				{
					if(argc>2) {
						hbill.curorderid = (atoi(argv[2]))-FIRSTORDERID;
						if(ord_key_exists_(&hbill, hbill.curorderid) == FALSE) {
							puts("Out of bound order id supplied!\n"); exit(0);
						}
						del_order_(&hbill);
						save_items_(&hbill);
						save_orders_(&hbill);
						show_orders_(&hbill);
					}
				}
				break;
			/*default:
				puts("Unknown option!\n");
				return -2;
				break;*/
		}
		opfound = TRUE;
	}

	/** Add items */
	if(opfound == FALSE) {
		if(strcmp(argv[1], "item") == 0) {
			if(argc>2) {
				if(strcmp(argv[2], "add") == 0) { // Add new item to inventory
					// This option checks if item exists and adds quantity if already exists else new record
					if(argc > 3) {
						val = argv[3];
						if(argc > 4)
							tot = atoi(argv[4]);
						else
							tot = 1;
						add_item_(&hbill, val, tot);
					}
				} else if(strcmp(argv[2], "edit") == 0) { // Edit an item name
					if(argc>3) {
						id = atoi(trim(argv[3], MAX_INPUT));
						hbill.invt.curitemid = id;
						if(argc>4) {
							if((strlen(argv[4])>1)
								&& (argv[4][0] == '-') 
								&& argc>5
							) {
								val = argv[5];
								switch(argv[4][1]) {
									case 't': // Name/title edit
										{
											if(argc>6)
												tot = atoi(argv[6]);
											else
												tot = hbill.invt.item[id-1].quantity;
											edit_item_(&hbill, id, val, tot);

										}
										break;
									case 'p': // Price
										{
											edit_item_price_(&hbill, id, atof(val));
										}
										break;
									case 'q':
										{
											edit_item_qty_(&hbill, id, atof(val));
										}
										break;
									case 'w':
										{
											edit_item_weight_(&hbill, id, atof(val));
										}
										break;
								}
								save_items_(&hbill);
							}
						}
					}
				} else if(strcmp(argv[2], "del") == 0) { // Remove item from inventory
					if(argc>3) {
						id = atoi(argv[3]);
						if(itm_ords_key_exists_(&hbill, id) == TRUE) {
							puts("This item exists in one of the orders. Invalid request!\n");
							exit(0);
						}
						hbill.invt.curitemid = id;
						if(del_item_(&hbill)==TRUE)
							puts("Item deleted successfully!\n");
						save_items_(&hbill);
					} else puts("ID missing\n");
				}
			}
			//save_items_(&hbill);
			show_items_(&hbill);
		} else if(strcmp(argv[1], "acc") == 0) { // Accounts
//			show_acc_(&hbill);
		} else if(strcmp(argv[1], "cmp") == 0) { // Companies
/*			if(argc>2) {
				if(argv[2][0] == '-') { // Only allow this
					sel = argv[2][1];

					if(argc > 3) {
						id = atoi(trim(argv[3], MAX_INPUT));
						hbill.curcompid = id;
					}
					switch(sel) {
						case 'n':
							if(argc>3) {
								val = argv[3];
								add_comp_(&hbill, val);
							}
							show_comps_(&hbill);
							break;
						case 'u':
							if(argc>5) {
								val = argv[5];
								if(strcmp(argv[4], "name") == 0) { // Name edit
									save_comp_(&hbill, 'n', val);
								} else if(strcmp(argv[4], "phon") == 0) { // Phone
									save_comp_(&hbill, 'p', val);
								} else if(strcmp(argv[4], "addr") == 0) { // Address
									save_comp_(&hbill, 'a', val);
								} else if(strcmp(argv[4], "emai") == 0) {
									save_comp_(&hbill, 'e', val);
								} else if(strcmp(argv[4], "code") == 0) {
									save_comp_(&hbill, 'c', val);
								}
							}
							show_comps_(&hbill);
							break;
						case 's': // Show single company
							break;
						case 'l':
							show_comps_(&hbill);
							break;
					}
				}
			}
*/		} else if(strcmp(argv[1], "cfg") == 0) { // Configuration
			if(argc>2) {
				if(argv[2][0] == '-') {
					sel = argv[2][1];
					switch(sel) {
						case 's': // Show config
							break;
						case 'u': // Update
							if(argc > 4) {
								val = argv[4];
								if(strcmp(argv[3], "nm") == 0) { // Edit store name
									strcpy(hbill.config.name, trim(val, MAX_INPUT));

								} else if(strcmp(argv[3], "id") == 0) { // ID
									strcpy(hbill.config.id, trim(val, MAX_INPUT));

								} else if(strcmp(argv[3], "adr") == 0) { // Address
									strcpy(hbill.config.addr, trim(val, MAX_INPUT));
								} else if(strcmp(argv[3], "esd") == 0) { // Est date
									strcpy(hbill.config.estdate, trim(val, MAX_INPUT));
								} else if(strcmp(argv[3], "stx") == 0)
									hbill.config.servicetax = atof(trim(val, MAX_INPUT));
								else if(strcmp(argv[3], "vat") == 0)
									hbill.config.vattax = atof(trim(val, MAX_INPUT));
								else if(strcmp(argv[3], "onm") == 0)
									strcpy(hbill.config.owner.name, trim(val, MAX_INPUT));
								else if(strcmp(argv[3], "oad") == 0)
									strcpy(hbill.config.owner.addr, trim(val, MAX_INPUT));
							}
							save_config_(&hbill);
							break;
					}
				}
			}
			show_config_(&hbill);
		}
	}

	//start__(&hbill);
	return 0;
}