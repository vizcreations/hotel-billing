/**
* @order header file for function prototypes
* @author VizCreations
*/

void show_order_(HBll *);
int print_order_(HBll *, boolean);
void show_orders_(HBll *);
boolean save_orderid(HBll *);
void save_custname(HBll *, char *);
void save_custemail(HBll *, char *);
void save_custaddr(HBll *, char *);
void save_custsex(HBll *, char);
void save_custage(HBll *, int);
boolean save_order(HBll *);
void del_order_(HBll *);
boolean ord_key_exists_(HBll *, int);
void scan_oline_(HBll *, char line[], int);
void fetch_order_(HBll *, int);
void reset_order(HBll *);
boolean save_orders_(HBll *);
