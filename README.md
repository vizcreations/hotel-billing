HBS Hotel billing:
==================
A small walkthrough of the Linux application for your hotel 
business.


About:
======
HBS Hotel billing is a shell program for Linux, useful 
to generate your business related bills or invoices.

HBS can handle your orders and inventory and saves all data
into flat CSV format for cross application access and also
can print your bills to file or a physical printer.


Installation:
=============
You have to understand how programs are installed from source
first. Here are the steps.

1. make all

Hehe, that was easy right. Of course, you will get some unknown
library errors for which you are solely responsible for fixing.

2. make install

The above installs into /usr/local/bin directory, so it's upto
the user if he wants something in the system's directory.


Usage:
======
Usually it's ./hbs [-option] [action] [id]

But, it's more than that. Here's few that can get you started

1. :~$ ./hbs -n // Add a record/bill
2. :~$ ./hbs -u [orderid] cn "[Customer name]" // Update a record customer name
3. :~$ ./hbs item add "IceCream" 20 // Add item to inventory
4. :~$ ./hbs -u [orderid] ia [ID of item] 10 // Add item to order
5. :~$ ./hbs -l // Lists out all orders
6. :~$ ./hbs -s [orderid] // Save to file
7. :~$ ./hbs -p [orderid] // Print using your default printer
8. :~$ ./hbs -d [orderid] // Deletes an order
9. :~$ ./hbs -u [orderid] ir [id of item in order] // Deletes an item from order
10. :~$ ./hbs item del [itemid] // Remove item from inventory
11. :~$ ./hbs -u [orderid] iq [ID of item] 20 // Update item quantity in order
12. :~$ ./hbs item edit [itemid] -p 15.00 // Update price of item in inventory
13. :~$ ./hbs cfg -u adr "Abbey Road, London" // Update store address


OpenSource:
===========
HBS is fully open source and invites C programmers and particularly
Linux programmers to have a look at this. It's new wine in old bottle
but the wine has some new ingredients. You will like them. ;-)


Manual:
=======
More coming in a man page.
Any questions mail to viju.kantah@gmail.com


~VizCreations

