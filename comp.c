/**
* @company source file with functions for hotel-billing
* @author VizCreations
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hbll.h"

void add_comp_(HBll *hbill, char *name) {
	// save company details
	int i = 0;
	int compcount;
	int compid = -1;
	CMP *comp;

	compcount = hbill->compcount;
	compid = compcount; // Because it's a new record
	++compid; // New item
	comp = &hbill->company[compid-1]; // Index
	sprintf(comp->id, "%d", compid);
	strip_newline(name, MAX_INPUT);
	strcpy(comp->name, name);
	strcpy(comp->phone, "0T-1B-2A");
	strcpy(comp->email, "TBA");
	strcpy(comp->addr, "TBA");
	strcpy(comp->code, "ab");

	++hbill->compcount;
	save_comps_(hbill);
}

void save_comp_(HBll *hbill, char op, char *val) {
	int compid = 0;
	CMP *comp;

	compid = hbill->curcompid;
	comp = &hbill->company[compid-1]; // Index
	strip_newline(val, MAX_INPUT);
	switch(op) {
		case 'n': // Name
			{
				comp->name[0] = '\0'; // Reset previous name
				strcpy(comp->name, val);
			}
			break;
		case 'p': // Phone
			{
				comp->phone[0] = '\0';
				strcpy(comp->phone, val);
			}
			break;
		case 'e': // Email
			{
				comp->email[0] = '\0';
				strcpy(comp->email, val);
			}
			break;
		case 'a': // Address
			{
				comp->addr[0] = '\0';
				strcpy(comp->addr, val);
			}
			break;
		case 'c':
			{
				comp->code[0] = '\0';
				strcpy(comp->code, val);
			}
			break;
	}

	save_comps_(hbill); // Update file
}

void show_acc_(HBll *hbill) {
	int i = 0, k = 0;
	int acount = 0; // Accounts total count including cr and dr
}

void show_comps_(HBll *hbill) {
	int i=0,j=0;
	CMP *comp;
	int compcnt = 0;
	compcnt = hbill->compcount;

	if(compcnt>0) {
		puts("ID\t\tCOMPANY NAME\t\tPHONE\t\tEMAIL\t\tADDRESS\n");
		while(i<compcnt) {
			comp = &hbill->company[i];
			printf("%s\t\t%s\t\t%s\t\t%s\t\t%s\n",
				comp->id, comp->name, comp->phone, comp->email, comp->addr);
			++i;
		}
	} else
		puts("No companies created!\n");
}


void save_comps_(HBll *hbill) { // Save to file
	int i = 0;
	int compcount = 0;
	FILE *fp;
	CMP *comp;

	compcount = hbill->compcount;
	if(file_exists_(COMPSFILE)) unlink(COMPSFILE);
	fp = fopen(COMPSFILE, "w+");
	if(fp) {
		while(i<compcount) {
			comp = &hbill->company[i];
			fprintf(fp, "%s, %s, %s, %s, %s\n",
				comp->id, comp->name, comp->phone, comp->email, comp->addr);
			++i;
		}
		fclose(fp);
	}
}

void scan_comp_(HBll *hbill, FILE *fp) {
	int i = 0;
	CMP *comp;
	int compcount = 0;
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	char addr[MAX_STR];
	char phone[MAX_INPUT];
	char email[MAX_INPUT];

	char line[MAX_LINE];
	char *token = NULL;
	char delim[] = ",";

	while(fgets(line, MAX_LINE+1, fp) != NULL) {
		token = strtok(line, delim);
		if(token != NULL)
			strcpy(id, trim(token, MAX_INPUT));
		else {
			puts("Failed reading companies file!\n"); exit(0);
		}
		token = strtok(NULL, delim);
		if(token != NULL)
			strcpy(name, trim(token, MAX_INPUT));
		token = strtok(NULL, delim);
		if(token != NULL)
			strcpy(phone, trim(token, MAX_INPUT));
		token = strtok(NULL, delim);
		if(token != NULL)
			strcpy(email, trim(token, MAX_INPUT));
		token = strtok(NULL, delim);
		if(token != NULL) {
			strip_newline(token, MAX_STR);
			strcpy(addr, trim(token, MAX_STR));
		}

		comp = &hbill->company[i];
		strcpy(comp->id, id);
		strcpy(comp->name, name);
		strcpy(comp->phone, phone);
		strcpy(comp->email, email);
		strcpy(comp->addr, addr);
		strcpy(comp->code, "ab");
		++i; ++compcount;
	}
	hbill->compcount = compcount;
}