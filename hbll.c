/**
* @common functions source file for hotel billing
* @author VizCreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
//#include <curl/curl.h>
#include "hbll.h"

int file_exists_(char *filename) {
	FILE *fp;
	boolean exists = FALSE;
	fp = fopen(filename, "r");
	if(fp) {
		exists = TRUE;
		fclose(fp);
	}
	return exists;
}

char *trim(char *str, int len) {
	char *newstr = malloc(1);
	int newlen = 0;
	char *trimmed;
	int i = 0, j = 0, k = 0;
	newstr = realloc(newstr, len);
	
	while(str[i] == ' ') // Skip spaces at the beginning
		++i;

	while(str[i] == '\t') // Skip tabs too
		++i;

	while(newstr[j++] = str[i]) // Creating new string
		++i;

	newstr[j] = '\0';
	newlen = j;

	/** Now try removing trailing spaces */
	while((newstr[newlen] == ' '))
		newlen--;

	while((newstr[newlen] == '\t')) // Remove tabs too
		newlen--;

	newstr[newlen] = '\0';
	strip_newline(newstr, newlen);
	//strcpy(trimmed, newstr);

	return newstr;
}

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			break;
		}
	}
}

int calculate_net_profit(HBll *hbill) {
	/**
	* @abstract We go through all records and add up original prices and tally with sold price
	*
	*
	*/
	ORD *order;
	int j = 0, k = 0;
	double sprice = 0.0;
	double oprice = 0.0;
	double netprofit = 0.0;

	if(hbill->ordercount > 0) {
/*		while(j++<hbill->ordercount) {
			order = &hbill->order[j];
			if(order->itemcount > 0) {
				while(k++<order->itemcount) {
					sprice += order->item[k].sprice;
					oprice += order->item[k].oprice;
				}
			}
		}
*/	}

	return netprofit;
}