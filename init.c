/**
* @init function source for hotel billing in Linux
* @author vizcreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include "hbll.h"

void init__(HBll *hbill) {
	// Grab data from files when they exist
	FILE *fp;
	char ordfilename[] = "hbill-orders.csv";
	char cstfilename[] = "hbill-customers.csv";
	char line[MAX_LINE];
	time_t ticks;

	/** Local variables to hold struct info temporarily */
	ORD *order;
	int oid;
	char itemname[MAX_INPUT];
	double itemprice;
	int itemqty;
	double itemwt;
	char custname[MAX_INPUT];
	char custemail[MAX_INPUT];
	char custaddr[MAX_STR];
	int custage;
	char custsex;
	char timestamp[MAX_INPUT];
	int ordercount = 0;
	int itemcount = 0;
	int lines = 0;

	/** Conf */
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	char addr[MAX_STR];
	char estdate[MAX_INPUT];
	double servicetax;
	double vattax;
	char oname[MAX_INPUT];
	char oaddr[MAX_STR];
	CFG *config;
	OWN *owner;

	strcpy(id, "ABC000 ``INVALID ID``");
	strcpy(name, "UNKNOWN ``INVALID NAME``");
	strcpy(addr, "UNKNOWN ``INVALID ADDRESS``");
	strcpy(estdate, "0000-00-00 00:00:00 ``INVALID DATE``");
	servicetax = 0.0;
	vattax = 0.0;

	strcpy(oname, "UNKNOWN ``INVALID OWNER NAME``");
	strcpy(oaddr, "UNKNOWN ``INVALID OWNER ADDRESS``");

	line[0] = '\0';
	lines = 0;
	fp = fopen(ITEMSFILE, "r");
	if(fp) {
		// READ file
		while(fgets(line, MAX_LINE, fp) != NULL) {
			scan_olineitem(hbill, line, lines);
			++lines;
		}
		fclose(fp);
	}

	fp = fopen(ordfilename, "r");
	if(fp) {
		// Begin reading and populating the structures
		while(fgets(line, MAX_LINE, fp) != NULL) {
			if(strlen(line) > 0) {
				scan_oline_(hbill, line, lines);
				++lines; // Orders
			}
		}
		ordercount += lines;
		itemcount = 1;
		fclose(fp);
	} else {
		/** Set everything to defaults */
		oid = 0;
		strcpy(itemname, "TBA");
		itemprice = 0.0;
		itemqty = 0;
		itemwt = 0.0;
		strcpy(custname, "TBA");
		strcpy(custemail, "TBA");
		strcpy(custaddr, "TBA");
		custage = 0;
		custsex = 'm';
		ticks = time(NULL);
		itemcount = 0;
		sprintf(timestamp, "%.24s", ctime(&ticks));
//		order = &hbill->order[lines];
		order = hbill->order;
		order = (ORD *) malloc(sizeof(ORD));
		sprintf(order->id, "%d", oid);
/*
		strcpy(order->item[0].name, itemname);
		order->item[0].sprice = itemprice;
		order->item[0].quantity = itemqty;
		order->item[0].weight = itemwt;
		order->itemcount = itemcount;
*/		strcpy(order->cust.name, custname);
		strcpy(order->cust.email, custemail);
		strcpy(order->cust.address, custaddr);
		order->cust.age = custage;
		order->cust.sex = custsex;
		hbill->curorderid = oid; // The last fetched
	}
/*
	fp = fopen(COMPSFILE, "r");
	if(fp) {
		scan_comp_(hbill, fp);
		fclose(fp);
	} else {
		hbill->compcount = 0;
		hbill->curcompid = -1; // No key in array
	}
*/
	/** Initiate configuration */
	fp = fopen(CONFIGFILE, "r");
	if(fp) {
		scan_config_(hbill, fp);
		fclose(fp);
	} else {

		config = &hbill->config;
		strcpy(config->id, id);
		strcpy(config->name, name);
		strcpy(config->addr, addr);
		strcpy(config->estdate, estdate);
		config->servicetax = servicetax;
		config->vattax = vattax;

		owner = &config->owner;
		strcpy(owner->name, oname);
		strcpy(owner->addr, oaddr);
	}

	hbill->ordercount = ordercount;
	hbill->invt.itemcount = lines;
//	hbill->totcompanies = 0;
	hbill->service_tax = 0.0;
	hbill->vat_tax = 0.0;

}