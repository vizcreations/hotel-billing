/**
* @store header file with prototypes for hotel-billing
* @author VizCreations
*/

void show_config_(HBll *);
void scan_config_(HBll *, FILE *);
boolean save_config_(HBll *);