/**
* @config header file for Hotel-Billing-solution
* @author vizcreations
* @declarations for macros and structures
*/

#define MAX_INPUT 255
#define MIN_INPUT 20
#define MAX_STR 2000
#define EXTRA 5
#define MAX_LINE MAX_STR+EXTRA
#define MAX_RECORDS 50
#define MAX_DATA 5000 // Entire data loaded to memory
#define MAX_FILEPATH 200
#define MAX_CMD 100

#define TRUE 1
#define FALSE -1

#define ORDERSFILE "hbill-orders.csv"
#define CUSTSFILE "hbill-customers.csv"
#define ITEMSFILE "hbill-items.csv"
#define COMPSFILE "hbill-companies.csv"

#define SHOW_ENV FALSE
#define SHOW_ARGV FALSE

#define FIRSTORDERID 10001
#define FIRSTOITEMID 1001

#define CONFIGFILE "store.cfg"
#define ROOTDIR '.'
#define DS '\\'
#define TMPDIR "tmp"
#define MODL_EXEC "hbll"

typedef short boolean;

typedef struct Customer {
	char name[MAX_INPUT];
	int age;
	char address[MAX_STR];
	char email[MAX_INPUT];
	char sex;
} CUST;

typedef struct Item {
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	double sprice;
	double oprice;
	double mrp;
	double weight;

	int quantity;
	struct Item *next; // Next in list (Inventory builds it)
} ITM;

typedef struct OrderItem {
	int oitemid;
	int oitemqty;
	struct OrderItem *next;
} OITEM;

typedef struct Order {
	char id[MAX_INPUT];
	CUST cust;
	OITEM *oitem; // First item, also points to the current item
	int itemcount;
	char timestamp[MAX_INPUT];
	struct Order *next; // Next in the linked-list
} ORD;

typedef struct Inventory {
	char id[MAX_INPUT];
	ITM *item; // First
	int itemcount;
	int curitemid;
	char timestamp[MAX_INPUT];
} INV;

typedef struct Account {
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	double balance;
	int credit;
	char type[MIN_INPUT]; // Cr or Dr
} ACC;

typedef struct Company {
	char id[MAX_INPUT];
	char name[MAX_INPUT];
	char phone[MAX_INPUT];
	char addr[MAX_STR];
	char email[MAX_INPUT];

	char code[MIN_INPUT];
} CMP;

typedef struct Owner {
	char name[MAX_INPUT];
	char addr[MAX_STR];
} OWN;

typedef struct Config {
	char id[MAX_INPUT]; // Store registration ID
	char name[MAX_INPUT]; // Store name
	char addr[MAX_STR]; // Store address
	char estdate[MAX_INPUT]; // Store established date
	double servicetax;
	double vattax;
	OWN owner;
} CFG;

typedef struct HotelBilling {
//	ORD order[MAX_RECORDS];
//	ACC account[MAX_RECORDS];
//	CMP company[MAX_RECORDS];
	ORD *order; // Points to first order
	int ordercount;
	int curorderid;
	int curcompid;
//	int compcount;

	/** Inventory */
	INV invt; // the items
//	int totcompanies; // Alias to compcount - UNUSED
	CFG config;

	/** Changeable fields */
	double service_tax;
	double vat_tax; // If store owner wants to take
} HBll;

/** Function prototypes definitions */
/** General */
int file_exists_(char *);
char *trim(char *, int);
void strip_newline(char *, int);
int calculate_net_profit(HBll *);

/** Data init */
void init__(HBll *);

/** Company specific */
/*
void add_comp_(HBll *, char *);
void save_comp_(HBll *, char, char *);
void show_acc_(HBll *);
void show_comps_(HBll *);
void save_comps_(HBll *);
void scan_comp_(HBll *, FILE *);
*/
/** Item in inventory */
int item_qty_overflow_(HBll *, int);
void save_item_(HBll *, int);
void edit_oitem_(HBll *, char *, int);
void save_item_qty_(HBll *, int);
int save_oitems_(HBll *);
void add_item_(HBll *, char *, int);
void edit_item_(HBll *, int, char *, int);
void edit_item_qty_(HBll *, int, int );
void edit_item_price_(HBll *, int, double);
void edit_item_weight_(HBll *, int, double);
int save_items_(HBll *);
int del_item_(HBll *);
void del_oitem(HBll *);
int itm_key_exists_(HBll *, int);
int itm_ord_key_exists_(HBll *, int);
int itm_ords_key_exists_(HBll *, int);
void scan_olineitem(HBll *, char *, int);
void show_items_(HBll *);

/** Orders */
void show_order_(HBll *);
int print_order_(HBll *, int);
void show_orders_(HBll *);
int save_orderid(HBll *);
void save_custname(HBll *, char *);
void save_custemail(HBll *, char *);
void save_custaddr(HBll *, char *);
void save_custsex(HBll *, char);
void save_custage(HBll *, int);
int save_order(HBll *);
void del_order_(HBll *);
int ord_key_exists_(HBll *, int);
void scan_oline_(HBll *, char line[], int);
void fetch_order_(HBll *, int);
void reset_order(HBll *);
int save_orders_(HBll *);

/** Store */
void show_config_(HBll *);
void scan_config_(HBll *, FILE *);
int save_config_(HBll *);

/** GPL */
void show_license();