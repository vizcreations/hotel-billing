# @Makefile for Hotel-Billing solution
# @author VizCreations

SRC_CIL = hbll.c comp.c init.c item.c order.c store.c gpl.c
OBJ_CIL = hbll.o comp.o init.o item.o order.o store.o gpl.o

CIL_INCLUDES = -I/usr/include -I. -I/usr/local/include
OBJ_INCLUDES = -L/usr/lib -L/usr/local/lib -lc

all: compile_cil binary
compile_cil:
	gcc -c $(SRC_CIL)
	ar rcs hbll.a $(OBJ_CIL)
	$(RM) *.o
binary:
	gcc -o hbs $(CIL_INCLUDES) main.c hbll.a $(CIL_LIBS)
install:
	cp hbs /usr/local/bin/

clean:
	$(RM) *BS *.o ~* *.csv *.a *.cfg tmp/*.waste
